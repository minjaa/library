﻿namespace Library.Business
{
    public class DeleteUserCommand : ICommand
    {
        public int Id { get; set; }
    }
}
