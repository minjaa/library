﻿namespace Library.Business
{
    public class InsertUserCommand : ICommand
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public int MaritalStatusId { get; set; }
    }
}
