﻿using System.Threading.Tasks;
using Library.Data.Models;
using Library.Repository.Users;

namespace Library.Business
{
    public class UserCommandHandler : 
        ICommandHandler<InsertUserCommand>,
        ICommandHandler<DeleteUserCommand>
    {
        private readonly IUserRepository _userRepository;

        public UserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Handler(InsertUserCommand insertUserCommand)
        {
            // Ovo prebacujemo u extension metodu
            // var userData = insertUserCommand.ToData();
            var userData = new User
            {
                Address = insertUserCommand.Address,
                FirstName = insertUserCommand.FirstName,
                MaritalStatusId = insertUserCommand.MaritalStatusId,
                LastName = insertUserCommand.LastName
            };

            await _userRepository.InsertUserAsync(userData);
        }

        public async Task Handler(DeleteUserCommand command)
        {
            await _userRepository.DeleteUserAsync(command.Id);
        }
    }
}
