﻿namespace Library.Common.Enums
{
    public enum Genre
    {
        Fantasy=1,
        ScienceFiction,
        Western,
        Romance,
        Thriller,
        Mystery,
        Biography,
        Horror,
        ChildrensLiterature,
        Science
    }
}
