﻿using Library.Common.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Data.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new LibraryContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<LibraryContext>>()))
            {
                if (!context.MaritalStatus.Any())
                {
                    var enums = Enum.GetValues(typeof(Maritial));

                    var maritalItems = new List<MaritalStatus>();
                    foreach (var item in enums)
                    {
                        var status = new MaritalStatus
                        {
                            Id = (int)item,
                            Caption = item.ToString()
                        };
                        maritalItems.Add(status);
                    }

                    context.MaritalStatus.AddRange(maritalItems);
                    context.SaveChanges();
                }

                if (context.Genre.Any())
                {
                    context.SaveChanges();

                    return;
                }

                var genreEnums = Enum.GetValues(typeof(Common.Enums.Genre));

                var genreItems = new List<Genre>();
                foreach(var item in genreEnums)
                {
                    var genre = new Genre
                    {
                        GenreId=(int)item,
                        Caption = item.ToString()
                    };

                    genreItems.Add(genre);
                }

                context.Genre.AddRange(genreItems);
                context.SaveChanges();
            }
        }
    }
}
