﻿using Library.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Repository.Books
{
    public interface IBookRepository : IDisposable
    {
        List<Book> GetBooksAsync();
        Task<Book> GetBookByIDAsync(int bookId);
        Task InsertBookAsync(Book book);
        Task DeleteBookAsync(int bookID);
        Task UpdateBookAsync(Book book);
    }
}
