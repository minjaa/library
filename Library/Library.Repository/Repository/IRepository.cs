﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Repository.Repository
{
    public interface IRepository<TEntity> : IReadOnlyRepository<TEntity>, IWriteRepository<TEntity>
    {

    }
}
