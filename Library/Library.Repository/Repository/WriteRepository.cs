﻿using Library.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Library.Repository.Repository
{
    public class WriteRepository<TEntity> : IWriteRepository<TEntity> 
        where TEntity : class
    {
        private LibraryContext _context = null;
        private DbSet<TEntity> table = null;

        public WriteRepository(LibraryContext context)
        {
            _context = context;
            table = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetQuery(Expression<Func<TEntity,bool>> predicate)
        {
            return table.Where(predicate);
        }

        public async Task InsertItemAsync(TEntity item)
        {
            //_context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Genre ON");
            await table.AddAsync(item);
            _context.SaveChanges();
            //_context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Genre OF");
        }

        public async Task UpdateItem(TEntity item)
        {
            table.Update(item);
            await _context.SaveChangesAsync();
        }
    }
}
