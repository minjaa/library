﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Business;
using Library.Common.Enums;
using Library.Common.Utils;
using Library.Data;
using Library.Data.Models;
using Library.Extensions;
using Library.Models.Books;
using Library.Models.Users;
using Library.Repository.Repository;
using Library.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Library.Controllers
{
    [Authorize]
    public class LibraryController : Controller
    {
        private readonly LibraryContext _context;
        private readonly IReadOnlyRepository<User> _userReadRepository;
        private readonly IWriteRepository<User> _userWriteRepository;
        private readonly IReadOnlyRepository<Book> _bookReadRepository;
        private readonly IWriteRepository<Book> _bookWriteRepository;

        public LibraryController(LibraryContext context)
        {
            _context = context;
            _bookReadRepository = new ReadOnlyRepository<Book>(context);
            _bookWriteRepository = new WriteRepository<Book>(context);
            _userReadRepository = new ReadOnlyRepository<User>(context);
            _userWriteRepository = new WriteRepository<User>(context);

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Books()
        {
            var books = _context.Books
                  .Include(book => book.BookGenres)
                  .ThenInclude(book => book.Genre)
                  .ToList();

            var booksVM = books
                .Where(book => book.DeleteDate == null)
                .Select(book => book.ToBookVMData());

            return View(booksVM);
        }


        public IActionResult AddBook()
        {
            var book = BookVM.GetBookWithGenres();
            return View(book);
        }

        [ValidFilter]
        [HttpPost]
        public async Task<IActionResult> AddBook(BookVM book)
        {
            var bookDb = book.ToBookData();
            bookDb.SubmittedBy = HttpContext.User.GetUserId();

            await _bookWriteRepository.InsertItemAsync(bookDb);

            return RedirectToAction(nameof(Books));
        }

        public async Task<IActionResult> EditBook(int id)
        {
            var bookVM = await _bookWriteRepository.GetQuery(book => book.BookId == id)
                                        .Select(book => book.ToBookVMData())
                                        .SingleOrDefaultAsync();
            return View(bookVM);
        }

        [ValidFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBook(BookVM bookVM)
        {
            var bookDb = bookVM.ToBookData();
            bookDb.SubmittedBy = HttpContext.User.GetUserId();
            _bookWriteRepository.UpdateItem(bookDb);

            return RedirectToAction(nameof(Books));
        }

        public ActionResult DeleteBook()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteBook(int id)
        {
            var bookDb = await _bookWriteRepository.GetQuery(book => book.BookId == id)
                                                  .SingleOrDefaultAsync();
            bookDb.DeleteDate = DateTime.UtcNow;
            await _bookWriteRepository.UpdateItem(bookDb);

            return RedirectToAction(nameof(Books));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public  IActionResult SearchBook(string SearchName)
        {
            var books = _context.Books
                .Include(book => book.BookGenres)
                .ThenInclude(book => book.Genre)
                .ToList();

            var booksVM = books
                .Where(book => book.DeleteDate == null 
                && book.Caption.ToLower() == SearchName.ToLower())
                .Select(book => book.ToBookVMData());

            return View(nameof(Books), booksVM);
        }

        public  IActionResult DownloadBook(int id)
        {
            var book = _bookReadRepository.GetQuery(item => item.BookId == id)
               .SingleOrDefault();

            return File(book.BookFile, "application/pdf");
        }

        public IActionResult BookDetails(int id)
        {
            var bookDb = _context.Books
                   .Include(book => book.BookGenres)
                   .ThenInclude(book => book.Genre)
                   .Where(book => book.BookId == id)
                   .SingleOrDefault();

            return View(bookDb.ToBookVMData());
        }

        public async Task<IActionResult> Users()
        {

            var usersVM = await _userReadRepository.GetQuery(item => item.DeleteDate == null)
                                               .Select(user => user.ToUserVMData())
                                               .ToListAsync();
            return View(usersVM);
        }

        public IActionResult AddUser()
        {
            var user = UserVM.GetUserWithMaritals();
            return View(user);
        }

        [ValidFilter]
        [HttpPost]
        public async Task<IActionResult> AddUser(UserVM userVM)
        {
            var existingUser = await _userWriteRepository.GetQuery(user => user.UserName == userVM.UserName)
                                                         .SingleOrDefaultAsync();

            if (existingUser != null)
            {
                ViewBag.Message = LibraryControllerMessages.UserNameExists;
                var maritalValues = Enums.GetValues<Maritial>();
                var newList = maritalValues.ToSelectList();
                userVM.MaritalStatuses = newList;
                userVM.UserName = String.Empty;

                return View(userVM);
            }

            var userData = userVM.ToUserData();

            var insertUserCommand = new InsertUserCommand
            {
                FirstName = userVM.FirstName,
                LastName = userVM.LastName,
                Address = userVM.Address,
                MaritalStatusId = userVM.MaritalStatusId
            };
         
            return RedirectToAction(nameof(Users));
        }

        [ValidFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUser(EditUserVM editUserVM)
        {
            await _userWriteRepository.GetQuery(user => user.Id == editUserVM.UserId)
                                                .UpdateAsync(user => new User
                                                {
                                                    Address = editUserVM.Address,
                                                    MaritalStatusId = editUserVM.MaritalStatusId,
                                                    InsertDate = DateTime.UtcNow,
                                                    PhoneNumber = editUserVM.PhoneNumber
                                                });

            return RedirectToAction(nameof(Users));
        }

        public async Task<IActionResult> EditUser(int id)
        {
            var userVM = await _userWriteRepository.GetQuery(user => user.Id == id)
                                                   .Select(user => user.ToEditUserVMData())
                                                   .SingleOrDefaultAsync();
            var maritalValues = Enums.GetValues<Maritial>();
            var maritalSelectList = maritalValues.ToSelectList();
            userVM.MaritalStatuses = maritalSelectList;

            return View(userVM);
        }

        public ActionResult DeleteUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUser(int id)
        {
            var userDb = await _userWriteRepository.GetQuery(user => user.Id == id)
                                                   .SingleOrDefaultAsync();
            userDb.DeleteDate = DateTime.UtcNow;


            var deleteUserCommand = new DeleteUserCommand
            {
                Id = id
            };

            return RedirectToAction(nameof(Users));
        }

        public  ActionResult DetailsUser(int id)
        {
            //var booksVM = await _bookReadRepository.GetQuery(book => book.SubmittedBy == id)
            //                                      .Where(book => book.DeleteDate == null)
            //                                      .Select(book => book.ToBookVMData())
            //                                      .ToListAsync();

            var books = _context.Books
                  .Include(book => book.BookGenres)
                  .ThenInclude(book => book.Genre)
                  .ToList();

            var booksVM = books
                .Where(book => book.DeleteDate == null && book.SubmittedBy==id)
                .Select(book => book.ToBookVMData());

            return View(booksVM);
        }

        
    }
}