﻿using Library.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Library.Extensions
{
    public static class UserManagerExtensions
    {
       
        public static int GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            return int.Parse(principal.FindFirst(ClaimTypes.NameIdentifier)?.Value);
        }
    }
}