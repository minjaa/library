﻿using Library.Common.Utils;
using Library.Data.Models;
using Library.Models.Books;
using Library.Models.Users;
using System.Collections.Generic;
using System.IO;

namespace Library.Extensions
{
    public static  class DataToVM
    {
        public static UserVM ToUserVMData(this User user)
        {
            var userVM =
                new UserVM
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Address = user.Address,
                    MaritalStatusId = user.MaritalStatusId,
                    PhoneNumber = user.PhoneNumber
                };

            return userVM;
        }

        public static EditUserVM ToEditUserVMData(this User user)
        {
            var userVM =
                new EditUserVM
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Address = user.Address,
                    MaritalStatusId = user.MaritalStatusId,
                    PhoneNumber = user.PhoneNumber
                };

            return userVM;
        }

        public static BookVM ToBookVMData(this Book book)
        {
            var genres = Enums.GetValues<Common.Enums.Genre>();
            var genreNames = string.Empty;

            foreach (var bookGenre in book.BookGenres)
            {
                genreNames += System.Enum.GetName(typeof(Common.Enums.Genre), bookGenre.GenreId)+" ";
            }

            var bookVM =
                new BookVM
                {
                    BookId = book.BookId,
                    Caption = book.Caption,
                    Description = book.Description,
                    Author = book.Author,
                    ReleaseDate = book.ReleaseDate,
                    Genres = genreNames
                };

            return bookVM;
        }
    }
}
