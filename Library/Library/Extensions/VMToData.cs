﻿using Library.Data.Models;
using Library.Models.Books;
using Library.Models.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace Library.Extensions
{
    public static class VMToData
    {

        public static User ToUserData(this UserVM user)
        {
            var userDb = new User
            {
                Id = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address,
                MaritalStatusId = user.MaritalStatusId,
                InsertDate = DateTime.UtcNow,
                UserName = user.UserName,
                IdNumber = 0,
                PhoneNumber = user.PhoneNumber,
                Email = user.Email
            };

            return userDb;
        }

        public static User ToEditUserData(this EditUserVM user)
        {
            var userDb = new User
            {
                Id = user.UserId,
                Address = user.Address,
                MaritalStatusId = user.MaritalStatusId,
                InsertDate = DateTime.UtcNow,
                UserName = user.UserName,
                IdNumber = 0,
                PhoneNumber = user.PhoneNumber,
            };

            return userDb;
        }

        public static Book ToBookData(this BookVM book)
        {
            var bookDb = new Book
            {
                Author = book.Author,
                Caption = book.Caption,
                InsertDate = DateTime.UtcNow,
                ReleaseDate = book.ReleaseDate,
                Description=book.Description
            };

            bookDb.BookGenres = new List<BookGenre>();

            foreach (var genreId in book.SelectedBookGenres)
            {
                bookDb.BookGenres.Add(new BookGenre
                {
                    BookId = book.BookId,
                    GenreId = genreId
                });
            }

            using (var memoryStream = new MemoryStream())
            {
                 book.BookFile.CopyTo(memoryStream);
                bookDb.BookFile = memoryStream.ToArray();
            };

            return bookDb;
        }
    }
}
