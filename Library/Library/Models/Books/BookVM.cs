﻿using Library.Common.Enums;
using Library.Common.Utils;
using Library.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Models.Books
{
    public class BookVM
    {
        public int BookId { get; set; }

        [Display(Name = "Title")]
        public string Caption { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }

        public string Author { get; set; }

        public DateTime InsertDate { get; set; }

        public DateTime DeleteDate { get; set; }

        public string User { get; set; }

        [Display(Name = "Book Genres")]
        public IEnumerable<SelectListItem> BookGenres { get; set; }

        //  ICollection<BookGenre> 
        public string Genres { get; set; }

        public List<int> SelectedBookGenres { get; set; }

        public string SearchName { get; set; }

        public string Description { get; set; }

        [Display(Name = "Upload book")]
        public IFormFile BookFile { get; set; }


        public BookVM()
        {

        }

        public BookVM(int bookId)
        {
            BookId = bookId;
        }

        public static BookVM GetBookWithGenres()
        {
            return new BookVM
            {
                BookGenres = Enums.GetValues<Genre>().ToSelectList()
            };
        }
    }
}
