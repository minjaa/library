﻿using Library.Common.Enums;
using Library.Common.Utils;
using Library.Extensions;
using Library.Models.BaseModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Library.Models.Users
{
    public class UserVM : BaseViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public int UserId { get; set; }

        public string Address { get; set; }

        public int MaritalStatusId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        [Display(Name = "Marital Status")]
        public IEnumerable<SelectListItem> MaritalStatuses { get; set; }

        public UserVM()
        {

        }

        public UserVM(int userId)
        {
            UserId = userId;
        }

        public static UserVM GetUserWithMaritals()
        {
            return new UserVM
            {
                MaritalStatuses = Enums.GetValues<Maritial>().ToSelectList()
            };
        }
    }
}
